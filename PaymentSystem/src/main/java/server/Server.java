package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import common.data.ConfirmationChoice;
import common.data.ConfirmationRequestData;
import common.data.ConfirmationResponseData;
import common.data.MenuData;
import common.data.MenuOptionData;
import common.data.TransactionData;
import common.data.TransactionErrorData;
import common.data.TransactionInitData;
import common.data.WalletData;
import common.entities.Transaction;
import common.entities.Transaction.TransactionStatus;
import common.entities.Transaction.TransactionType;
import common.entities.Wallet;
import common.message.PSMessageListAdapter;
import common.message.PSJsonSerializer;
import common.message.PSMessage;
import common.message.PSMessage.MessageType;
import common.message.PSMessageAdapter;
import common.message.PSMessageIO;
import common.message.PSMessageList;
import server.db.PSDatabase;

public class Server {
	
	private Wallet wallet;
	
	private PSMessageIO msgIO;
	
	private Gson gson;
	
	private ServerSocket serverSoc;
	
	private Socket soc;
	
	private PSDatabase db;
	
	public Server(ServerSocket serverSoc, PSMessageIO msgIO, Gson gson, PSDatabase db) {
		this.serverSoc = serverSoc;
		this.msgIO = msgIO;
		this.gson = gson;
		this.soc = null;
		this.wallet = new Wallet();
		this.db = db;
	}
	
	public void accept() throws IOException {
		this.soc = this.serverSoc.accept();
	}
	
	private void receiveWallet() throws IOException {
		PSMessage walletMsg = msgIO.readMessage(this.soc.getInputStream());
		WalletData walletData = (WalletData)walletMsg.getData(); 
		this.wallet.setID(walletData.getID());
		this.wallet.setBalance(walletData.getBalance());
	}
	
	private MenuOptionData createViewPendingSubmenu(List<Transaction> pendingTransactions) {
		if (pendingTransactions.isEmpty()) {
			return null;
		}
		MenuOptionData viewPendingOp = null;
		MenuData submenu = new MenuData();
		viewPendingOp = MenuOptionData.builder()
				.withId(3)
				.withDisplay("View pending transactions")
				.withSubmenu(submenu)
				.build();
		int i = 0;
		for (Transaction trans : pendingTransactions) {					
			ConfirmationResponseData respData = new ConfirmationResponseData(ConfirmationChoice.YES, trans.getData());
			PSMessage respMsg = new PSMessage(MessageType.CONFIRMATION_RESPONSE, respData);
			JsonObject respMsgObj = this.gson.toJsonTree(respMsg).getAsJsonObject();
			MenuOptionData pendingTransOp = MenuOptionData.builder()
					.withId(i)
					.withDisplay(trans.getType().name() + " transaction from " + trans.getSourceWalletId() + " for $" + String.format("%.2f", trans.getAmount()/100.0))
					.withResponse(respMsgObj)
					.withPrompt("Enter a choice (all caps): YES, NO, or LATER", 
							MessageType.CONFIRMATION_RESPONSE + ".choice")
					.build();
			viewPendingOp.getSubmenu().addOption(pendingTransOp);	
			i++;
		}
		
		return viewPendingOp;
	}
	
	private MenuData createMainMenu() {
		MenuData mainMenu = new MenuData();
		
		TransactionInitData paymentInitData = new TransactionInitData(this.wallet.getID(), 0, TransactionType.PAYMENT, 0);
		PSMessage paymentTransInitMsg = new PSMessage(PSMessage.MessageType.TRANSACTION_INIT, paymentInitData);
		JsonObject paymentInitTransObj = gson.toJsonTree(paymentTransInitMsg).getAsJsonObject();
		
		TransactionInitData billInitData = new TransactionInitData(this.wallet.getID(), 0, TransactionType.BILL, 0);
		PSMessage billTransInitMsg = new PSMessage(PSMessage.MessageType.TRANSACTION_INIT, billInitData);
		JsonObject billInitTransObj = gson.toJsonTree(billTransInitMsg).getAsJsonObject();
		
		MenuOptionData initPaymentOp = MenuOptionData.builder()
				.withId(1)
				.withDisplay("Initiate Payment Transaction")
				.withResponse(paymentInitTransObj)
				.withNumberPrompt("Enter the destination wallet id", MessageType.TRANSACTION_INIT + ".destination_id")
				.withNumberPrompt("Enter the amount in cents", MessageType.TRANSACTION_INIT + ".amount")
				.build();
		
		MenuOptionData initBillOp = MenuOptionData.builder()
				.withId(2)
				.withDisplay("Initiate Bill Transaction")
				.withResponse(billInitTransObj)
				.withNumberPrompt("Enter the destination wallet id", MessageType.TRANSACTION_INIT + ".destination_id")
				.withNumberPrompt("Enter the amount in cents", MessageType.TRANSACTION_INIT + ".amount")
				.build();
				
		mainMenu.addOption(initPaymentOp);
		mainMenu.addOption(initBillOp);
		
		return mainMenu;
	}
	
	private TransactionErrorData validateMessage(PSMessage msg) {
		TransactionErrorData err = null;
		switch (msg.getMessageType()) {
		case TRANSACTION_INIT:
			TransactionInitData data = (TransactionInitData)msg.getData();
			if (data.getAmount() <= 0) {
				err = new TransactionErrorData("The transaction amount must be positive", 0);
			} else if (data.getType().equals(TransactionType.PAYMENT) 
					&& data.getSourceId() == this.wallet.getID() 
					&& data.getAmount() > this.wallet.getBalance()) {

				err = new TransactionErrorData("You don't have that much money", 0);
			}
			break;
		case CONFIRMATION_RESPONSE:
			ConfirmationResponseData respData = (ConfirmationResponseData)msg.getData();
			TransactionData transData = respData.getTransaction();
			ConfirmationChoice choice = respData.getChoice();
			
			//Make sure the wallet has enough money for the transaction
			//If is source and type is Payment, or is dest and type is Bill
			if (choice.equals(ConfirmationChoice.YES)
					&& ((transData.getType().equals(TransactionType.PAYMENT) 
							&& transData.getSourceId() == this.wallet.getID()) //Payment too large to confirm
						|| (transData.getType().equals(TransactionType.BILL) 
							&& transData.getDestinationId() == this.wallet.getID())) //Bill too large to accept
					&& transData.getAmount() > this.wallet.getBalance()) {
				err = new TransactionErrorData("You don't have that much money", 0);
			}
			break;
		default:
			break;
		}
		
		return err;
	}
	
	private MenuOptionData createOptionFromTransaction(Transaction trans, MenuOptionData viewPendingOp) {
		ConfirmationResponseData respData = new ConfirmationResponseData(ConfirmationChoice.YES, trans.getData());
		PSMessage respMsg = new PSMessage(MessageType.CONFIRMATION_RESPONSE, respData);
		JsonObject respMsgObj = this.gson.toJsonTree(respMsg).getAsJsonObject();
		MenuOptionData pendingTransOp = MenuOptionData.builder()
				.withId(viewPendingOp.getSubmenu().getOptions().size())
				.withDisplay(trans.getType().name() + " transaction from " + trans.getSourceWalletId() + "  for $" + String.format("%.2f", trans.getAmount()/100.0))
				.withResponse(respMsgObj)
				.withPrompt("Enter a choice (all caps): YES, NO, or LATER", 
						MessageType.CONFIRMATION_RESPONSE + ".choice")
				.build();
		
		return pendingTransOp;
	}
	
	private void processSourcePendingTransaction(Transaction trans, MenuData mainMenu,  MenuOptionData viewPendingOp) {
		TransactionStatus status = trans.getStatus();
		TransactionType type = trans.getType();
		
		if (type.equals(TransactionType.BILL) && status.equals(TransactionStatus.DEST_CONFIRMED)) {
			
			//Destination user confirmed the bill, so add the money to the source wallet
			this.wallet.setBalance(this.wallet.getBalance() + trans.getAmount());
			this.db.deleteTransaction(trans);
			
		} else if (type.equals(TransactionType.PAYMENT) && status.equals(TransactionStatus.DEST_DENIED)) {
			
			//the destination user denied the payment, so put the money back in the source
			this.wallet.setBalance(this.wallet.getBalance() + trans.getAmount());
			this.db.deleteTransaction(trans);
			
		} else if (status.equals(TransactionStatus.SOURCE_PENDING)) {
			//Add it to the menu for confirmation
			if (viewPendingOp == null) {
				MenuData submenu = new MenuData();
				viewPendingOp = MenuOptionData.builder()
						.withId(3)
						.withDisplay("View pending transactions")
						.withSubmenu(submenu)
						.build();
				mainMenu.addOption(viewPendingOp);
			}
			
			MenuOptionData pendingTransOp = this.createOptionFromTransaction(trans, viewPendingOp);
			
			viewPendingOp.getSubmenu().addOption(pendingTransOp);
		}
	}
	
	private void processDestPendingTransaction(Transaction trans, MenuData mainMenu,  MenuOptionData viewPendingOp) {
		TransactionStatus status = trans.getStatus();
		if (status.equals(TransactionStatus.SOURCE_CONFIRMED) || status.equals(TransactionStatus.DEST_PENDING)) {
			if (status.equals(TransactionStatus.SOURCE_CONFIRMED)) {
				//Only update the status if first time seeing this transaction (as dest)
				trans.setStatus(TransactionStatus.DEST_INIT);
				this.db.updateTransaction(trans);
			}
			//Add it to the menu for confirmation
			if (viewPendingOp == null) {
				MenuData submenu = new MenuData();
				viewPendingOp = MenuOptionData.builder()
						.withId(3)
						.withDisplay("View pending transactions")
						.withSubmenu(submenu)
						.build();
				mainMenu.addOption(viewPendingOp);
			}
			
			MenuOptionData pendingTransOp = this.createOptionFromTransaction(trans, viewPendingOp);
			
			viewPendingOp.getSubmenu().addOption(pendingTransOp);
		}
	}
	
	private void processInitialPendingTransactions(List<Transaction> pendingTransactions, MenuData mainMenu,  MenuOptionData viewPendingOp) throws IOException {
		for (int i = 0; pendingTransactions != null && i < pendingTransactions.size(); i++) {
			Transaction trans = pendingTransactions.get(i);
			if (trans.getSourceWalletId() == this.wallet.getID()) {
				this.processSourcePendingTransaction(trans, mainMenu, viewPendingOp);
			} else if (trans.getDestWalletId() == this.wallet.getID()) {
				this.processDestPendingTransaction(trans, mainMenu, viewPendingOp);
			}
			
			//Send the updated transaction
			PSMessage msg = new PSMessage(MessageType.TRANSACTION, trans);
			this.msgIO.writeMessage(this.soc.getOutputStream(), msg);
		}
	}
	
	private void updateAndSendTransaction(Transaction trans) throws IOException {
		this.db.updateTransaction(trans);
		PSMessage transMsg = new PSMessage(MessageType.TRANSACTION, trans);
		this.msgIO.writeMessage(this.soc.getOutputStream(), transMsg);
	}
	
	public void runProtocol() throws IOException {
		try {
			this.receiveWallet();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		//New wallet, make new one
		PSMessageList msgList = new PSMessageList();
		
		
		MenuData mainMenu = null;
		
		MenuOptionData viewPendingOp = null;
		
		List<Transaction> pendingTransactions = new ArrayList<>();
		
		if (this.wallet.getID() == 0) {
			this.wallet = this.db.insertWallet(wallet);
			mainMenu = this.createMainMenu();
		} else {
			mainMenu = this.createMainMenu();
			pendingTransactions = this.db.getPendingTransactions(this.wallet);
			
			//Modify the wallet and transactions if needed
			//Also add to the menu if actions are needed (e.g. confirmation, rejection)
			this.processInitialPendingTransactions(pendingTransactions, mainMenu, viewPendingOp);
		}
		PSMessage walletMsg = new PSMessage(MessageType.WALLET, this.wallet);
		msgList.addMessage(walletMsg);
		
		PSMessage menuMsg = new PSMessage(MessageType.MENU, mainMenu);
		msgList.addMessage(menuMsg);
		
		//Send the updated wallet and menu
		this.msgIO.writeMessage(this.soc.getOutputStream(), msgList);
		
		PSMessage transMsg = null;
		
		while (true) {
			PSMessage clientMsg = null;
			try {
				clientMsg = this.msgIO.readMessage(this.soc.getInputStream());
			} catch (Exception e) {
				break;
			}
			
			TransactionErrorData errData = this.validateMessage(clientMsg);
			if (errData != null) {
				//Send the error message and menu
				PSMessage errMsg = new PSMessage(MessageType.TRANSACTION_ERROR, errData);
				msgList = new PSMessageList();
				msgList.addMessage(errMsg);
				msgList.addMessage(menuMsg);
				this.msgIO.writeMessage(this.soc.getOutputStream(), msgList);
				
			} else {
				switch (clientMsg.getMessageType()) {
				case TRANSACTION_INIT:
					TransactionInitData data = (TransactionInitData)clientMsg.getData();
					
					//Create a new transaction
					Transaction newTrans = new Transaction(0, 
							data.getType(), 
							data.getAmount(), 
							data.getSourceId(), 
							data.getDestinationId(),
							TransactionStatus.SOURCE_INIT,
							Instant.now());
					
					//newTrans now has new Id
					newTrans = this.db.insertTransaction(newTrans);
					
					//Create a confirmation message
					ConfirmationRequestData reqData = new ConfirmationRequestData(newTrans.getData(), ConfirmationChoice.values());
					PSMessage reqMsg = new PSMessage(PSMessage.MessageType.CONFIRMATION_REQUEST, reqData);
					transMsg = new PSMessage(MessageType.TRANSACTION, newTrans);
					
					msgList = new PSMessageList();
					msgList.addMessage(reqMsg);
					msgList.addMessage(transMsg);
					this.msgIO.writeMessage(this.soc.getOutputStream(), msgList);
					
					break;
				case CONFIRMATION_RESPONSE:
					ConfirmationResponseData respData = (ConfirmationResponseData)clientMsg.getData();
					
					boolean isSource = respData.getTransaction().getSourceId() == this.wallet.getID();
					boolean isDest = respData.getTransaction().getDestinationId() == this.wallet.getID();
					
					Transaction trans = new Transaction(respData.getTransaction().getID(),
							respData.getTransaction().getType(),
							respData.getTransaction().getAmount(),
							respData.getTransaction().getSourceId(),
							respData.getTransaction().getDestinationId(),
							respData.getTransaction().getStatus(),
							respData.getTransaction().getCreatedAt());
					
					if (isDest) {
						switch (respData.getTransaction().getType()) {
						case PAYMENT:
							switch (respData.getChoice()) {
							case YES:
								//Update the transaction
								trans.setStatus(TransactionStatus.DEST_CONFIRMED);
								this.db.updateTransaction(trans);
								
								//Send the updated wallet and transaction to the client
								msgList = new PSMessageList();
								transMsg = new PSMessage(MessageType.TRANSACTION, trans);
								
								this.wallet.setBalance(this.wallet.getBalance() + trans.getAmount());
								walletMsg = new PSMessage(MessageType.WALLET, this.wallet);
								
								msgList.addMessage(transMsg);
								msgList.addMessage(walletMsg);
								
								System.out.println("Sending wallet and transaction");
								
								this.msgIO.writeMessage(this.soc.getOutputStream(), msgList);
								break;
							case NO:
								trans.setStatus(TransactionStatus.DEST_DENIED);
								this.updateAndSendTransaction(trans);
								break;
							case LATER:
								trans.setStatus(TransactionStatus.DEST_PENDING);
								this.updateAndSendTransaction(trans);
								break;
							}
							break;
						case BILL:
							switch (respData.getChoice()) {
							case YES:
								//Update the transaction
								trans.setStatus(TransactionStatus.DEST_CONFIRMED);
								this.db.updateTransaction(trans);
								
								//Send the updated wallet and transaction to the client
								msgList = new PSMessageList();
								transMsg = new PSMessage(MessageType.TRANSACTION, trans);
								
								this.wallet.setBalance(this.wallet.getBalance() - trans.getAmount());
								walletMsg = new PSMessage(MessageType.WALLET, this.wallet);
								
								msgList.addMessage(transMsg);
								msgList.addMessage(walletMsg);
								
								System.out.println("Sending wallet and transaction");
								
								this.msgIO.writeMessage(this.soc.getOutputStream(), msgList);
								break;
							case NO:
								trans.setStatus(TransactionStatus.DEST_DENIED);
								this.updateAndSendTransaction(trans);
								break;
							case LATER:
								trans.setStatus(TransactionStatus.DEST_PENDING);
								this.updateAndSendTransaction(trans);
								break;
							}
							break;
						default:
							break;
						}
					} else if (isSource) {
						
						switch (respData.getTransaction().getType()) {
						case PAYMENT:
							switch (respData.getChoice()) {
							case YES:
								
								//Update the transaction
								trans.setStatus(TransactionStatus.SOURCE_CONFIRMED);
								this.db.updateTransaction(trans);
								
								//Send the updated wallet and transaction to the client
								msgList = new PSMessageList();
								transMsg = new PSMessage(MessageType.TRANSACTION, trans);
								
								this.wallet.setBalance(this.wallet.getBalance() - trans.getAmount());
								walletMsg = new PSMessage(MessageType.WALLET, this.wallet);
								
								msgList.addMessage(transMsg);
								msgList.addMessage(walletMsg);
								
								System.out.println("Sending wallet and transaction");
								
								this.msgIO.writeMessage(this.soc.getOutputStream(), msgList);
								break;
							case NO:
								trans.setStatus(TransactionStatus.SOURCE_DENIED);
								this.updateAndSendTransaction(trans);
								this.db.deleteTransaction(trans);
								break;
							case LATER:
								trans.setStatus(TransactionStatus.SOURCE_PENDING);
								this.updateAndSendTransaction(trans);
								break;
							}
							break;
						case BILL:
							switch (respData.getChoice()) {
							case YES:
								trans.setStatus(TransactionStatus.SOURCE_CONFIRMED);
								break;
							case NO:
								trans.setStatus(TransactionStatus.SOURCE_DENIED);
								this.db.deleteTransaction(trans);
								break;
							case LATER:
								trans.setStatus(TransactionStatus.SOURCE_PENDING);
								break;
							}
							
							this.updateAndSendTransaction(trans);
							break;
						default:
							break;
						}
					}
					
					mainMenu = this.createMainMenu();
					
					pendingTransactions = this.db.getPendingTransactions(wallet)
							.stream()
							.filter((transaction) -> {
								boolean source = this.wallet.getID() == transaction.getSourceWalletId();
								boolean dest = this.wallet.getID() == transaction.getDestWalletId();
								TransactionStatus status = transaction.getStatus();
								/*
								 * Keep transaction if
								 * a) is source wallet and transaction is pending source action
								 * b) OR is dest wallet and transaction is pending dest action or source has confirmed
								 * Essentially, keep only transactions that need action from current wallet.
								 */
								return (source && status.equals(TransactionStatus.SOURCE_PENDING))
										|| (dest 
											&& (status.equals(TransactionStatus.DEST_PENDING) 
												|| status.equals(TransactionStatus.SOURCE_CONFIRMED)));
							})
							.collect(Collectors.toList());
					
					viewPendingOp = this.createViewPendingSubmenu(pendingTransactions);
					
					if (viewPendingOp != null) {
						mainMenu.addOption(viewPendingOp);
					}
					
					menuMsg = new PSMessage(MessageType.MENU, mainMenu);
					this.msgIO.writeMessage(this.soc.getOutputStream(), menuMsg);
					break;
				default:
					break;
				}
			}
		}
	}

	public static void main(String[] args) throws InterruptedException {
		int port = Integer.parseInt(args[0]);
		
		Gson gson = new GsonBuilder()
				.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
				.registerTypeAdapter(PSMessage.class, new PSMessageAdapter())
				.registerTypeAdapter(PSMessageList.class, new PSMessageListAdapter())
				.create();
		
		PSJsonSerializer ser = new PSJsonSerializer(gson);
		
		PSMessageIO msgIO = new PSMessageIO(ser);
		
		//Initialize SQLite connection and tables. Only creates new table if they don't already exist
		PSDatabase db = new PSDatabase("server.db");
		db.createWalletTable();
		db.createTransactionTable();
		
		
		try {
			ServerSocket serverSoc = new ServerSocket(port, 5);
			Server server = new Server(serverSoc, msgIO, gson, db);
			System.out.println("Server listening on port: " + port);
			
			//Have flag for easier testing, could be used if/when server is expanding
			boolean done = false;
			while (!done) {
				server.accept();
				server.runProtocol();
			}			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
}
