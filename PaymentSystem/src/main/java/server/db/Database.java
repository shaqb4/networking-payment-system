package server.db;

import java.sql.Connection;

public interface Database {
	Connection connect();
	
	void close();
}
