package server.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import common.entities.Transaction;
import common.entities.Transaction.TransactionStatus;
import common.entities.Transaction.TransactionType;
import common.entities.Wallet;

public class PSDatabase implements Database {
	public static enum Table {
		WALLETS,
		TRANSACTIONS;
		
		@Override public String toString() {
			return name().toLowerCase();
		}
	}
	
	private String dbUrl;
	
	public PSDatabase(String path) {
		this.dbUrl = "jdbc:sqlite:" + path;
	}
	
	public Connection connect() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(this.dbUrl);
			System.out.println("Connected to database: " + this.dbUrl);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return conn;
	}
	
	public void close() {
		//SQLite best practice is apparently to open and close the connection for each function/transaction to save memory
		//So nothing to close here
	}
	
	public void createWalletTable() {
		String sql = "CREATE TABLE IF NOT EXISTS wallets (\n"
				+ "id integer PRIMARY KEY );";
		
		try (Connection conn = this.connect();
				Statement stmt = conn.createStatement()) {
			stmt.execute(sql);
			
			System.out.println("Created wallets table");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void createTransactionTable() {
		String sql = "CREATE TABLE IF NOT EXISTS transactions (\n"
				+ "id INTEGER PRIMARY KEY,\n"
				+ "type TEXT NOT NULL,\n"
				+ "amount INTEGER NOT NULL,\n"
				+ "source_id INTEGER NOT NULL,\n"
				+ "destination_id INTEGER NOT NULL,\n"
				+ "status TEXT NOT NULL,\n"
				+ "createdAt TEXT NOT NULL );";
		
		try (Connection conn = this.connect();
				Statement stmt = conn.createStatement()) {
			stmt.execute(sql);
			System.out.println("Created transactions table");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	private boolean entityExists(long id, String table) {
		String sql = "SELECT id  FROM " + table +  " WHERE id = ?";

		boolean entityExists = false;
		try (Connection conn = this.connect();
				PreparedStatement pstmt = conn.prepareStatement(sql)) {
			pstmt.setLong(1, id);
			
			System.out.println("Checking if entity exists");
			try (ResultSet rs = pstmt.executeQuery()) {
				entityExists = rs.isBeforeFirst();
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return entityExists;
	}
	
	private void deleteEntity(long id, String table) {
		String sql = "DELETE FROM " + table + " WHERE id = ?";
		
		try (Connection conn = this.connect();
				PreparedStatement pstmt = conn.prepareStatement(sql)) {
			pstmt.setLong(1, id);
			
			pstmt.executeUpdate();
			
			System.out.println("Deleting entity");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	private List<Transaction> resultSetToTransactions(ResultSet rs) {
		List<Transaction> transactions = new ArrayList<>();
		if (rs != null) {
			transactions = new ArrayList<>();
			try {				
				while (rs.next()) {
					transactions.add(new Transaction(rs.getLong("id"), 
							TransactionType.valueOf(rs.getString("type")), 
							rs.getLong("amount"),
							rs.getLong("source_id"),
							rs.getLong("destination_id"),
							TransactionStatus.valueOf(rs.getString("status")),
							Instant.parse(rs.getString("createdAt"))));
				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return transactions;
	}
	
	public Transaction insertTransaction(Transaction transaction) {
		String sql = "INSERT INTO transactions(type, amount, source_id, destination_id, status, createdAt) VALUES(?, ?, ?, ?, ?, ?);";
		long newId = transaction.getID();
		
		try (Connection conn = this.connect();
				PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			pstmt.setString(1, transaction.getType().name());
			pstmt.setLong(2, transaction.getAmount());
			pstmt.setLong(3, transaction.getSourceWalletId());
			pstmt.setLong(4, transaction.getDestWalletId());
			pstmt.setString(5, transaction.getStatus().name());
			pstmt.setString(6, transaction.getCreatedAt().toString());
			
			pstmt.executeUpdate();
			
			try (ResultSet keys = pstmt.getGeneratedKeys()) {
				if (keys.next()) {
					newId = keys.getLong(1);
				}
			}
			
			System.out.println("Inserting transaction");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return new Transaction(newId, 
				transaction.getType(), 
				transaction.getAmount(), 
				transaction.getSourceWalletId(), 
				transaction.getDestWalletId(), 
				transaction.getStatus(),
				transaction.getCreatedAt());
	}
	
	public boolean transactionExists(Transaction transaction) {
		System.out.println("Checking if transaction exists");
		return this.entityExists(transaction.getID(), Table.TRANSACTIONS.toString());
	}
	
	public void deleteTransaction(Transaction transaction) {
		System.out.println("Deleting transaction");
		this.deleteEntity(transaction.getID(), Table.TRANSACTIONS.toString());
	}
	
	public Transaction getTransaction(Transaction transaction) {
		String sql = "SELECT * FROM transactions WHERE id = ?";
		
		Transaction trans = null;
		
		try (Connection conn = this.connect();
				PreparedStatement pstmt = conn.prepareStatement(sql)) {
			pstmt.setLong(1, transaction.getID());
			
			ResultSet rs = pstmt.executeQuery();
			System.out.println("Getting transaction");
			if (rs.next()) {
				trans = new Transaction(rs.getLong("id"), 
						TransactionType.valueOf(rs.getString("type")), 
						rs.getLong("amount"),
						rs.getLong("source_id"),
						rs.getLong("destination_id"),
						TransactionStatus.valueOf(rs.getString("status")),
						Instant.parse(rs.getString("createdAt")));
			}
			System.out.println("Getting entity");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return trans;
	}
	
	public List<Transaction> getPendingTransactions(Wallet wallet) {
		List<Transaction> transactions = new ArrayList<>();
		
		String sql = "SELECT * FROM transactions WHERE source_id = ? OR destination_id = ?;";
		
		try (Connection conn = this.connect();
				PreparedStatement pstmt = conn.prepareStatement(sql)) {
			pstmt.setLong(1, wallet.getID());
			pstmt.setLong(2, wallet.getID());
			
			ResultSet rs = pstmt.executeQuery();
			if (rs.isBeforeFirst()) {
				transactions = this.resultSetToTransactions(rs);
			}
			
			System.out.println("Getting pending transactions");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return transactions;
	}
	
	public void updateTransaction(Transaction transaction) {
		String sql = "UPDATE transactions SET type = ?, "
					+ "amount = ?, "
					+ "source_id = ?, "
					+ "destination_id = ?, "
					+ "status = ? "
					+ "WHERE id = ?";
		
		try (Connection conn = this.connect();
				PreparedStatement pstmt = conn.prepareStatement(sql)) {
			
			pstmt.setString(1, transaction.getType().name());
			pstmt.setLong(2, transaction.getAmount());
			pstmt.setLong(3, transaction.getSourceWalletId());
			pstmt.setLong(4, transaction.getDestWalletId());
			pstmt.setString(5, transaction.getStatus().name());
			pstmt.setLong(6, transaction.getID());
			
			pstmt.executeUpdate();
			System.out.println("Updating transaction");
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public Wallet insertWallet(Wallet wallet) {
		String sql = "INSERT INTO wallets DEFAULT VALUES;";
		long newId = wallet.getID();
		
		try (Connection conn = this.connect();
				PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			pstmt.executeUpdate();
			try (ResultSet keys = pstmt.getGeneratedKeys()) {
				if (keys.next()) {
					newId = keys.getLong(1);
				}
			}
			System.out.println("Inserting wallet");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return new Wallet(newId, wallet.getBalance(), wallet.getTransactions());
	}
	
	public boolean walletExists(Wallet wallet) {
		System.out.println("Checking if wallet exists");
		return this.entityExists(wallet.getID(), Table.WALLETS.toString());
	}
	
	public void deleteWallet(Wallet wallet) {
		System.out.println("Deleting wallet");
		this.deleteEntity(wallet.getID(), Table.WALLETS.toString());
	}
}
