package client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import common.data.AbstractData;
import common.data.ConfirmationChoice;
import common.data.ConfirmationRequestData;
import common.data.ConfirmationResponseData;
import common.data.MenuData;
import common.data.MenuOptionData;
import common.data.TransactionData;
import common.data.TransactionErrorData;
import common.data.WalletData;
import common.entities.Transaction;
import common.entities.Wallet;
import common.message.PSMessageList;
import common.message.PSMessageListAdapter;
import common.message.PSJsonSerializer;
import common.message.PSMessage;
import common.message.PSMessageAdapter;
import common.message.PSMessageIO;


public class Client {
	
	public static enum ClientActions { ADD_CONTACT, REMOVE_CONTACT, LIST_CONTACTS, VIEW_WALLET_INFO, VIEW_TRANSACTIONS, LOGOUT };
	
	private Map<String, Long> contacts;
	
	private Wallet wallet;
	
	private PSMessageIO messageIO;
	
	private Socket socket;
	
	private MenuData clientMenu;
	
	
	public Client(Socket soc, PSMessageIO msgIO) throws UnknownHostException, IOException {
		
		this.socket = soc;
		
		this.messageIO = msgIO;
		
		this.contacts = new Hashtable<>();
		
		//init to default wallet with ID = 0. Server creates new wallet if and sends it to client 
		//if it receives a wallet with ID = 0
		this.wallet = new Wallet();  
		
		MenuOptionData clientOp = new MenuOptionData(0, "Client Actions", null);
		MenuOptionData addContactOp = new MenuOptionData(ClientActions.ADD_CONTACT.ordinal(), "Add contact", null);
		addContactOp.addPrompt("Enter contact name: ");
		addContactOp.addPrompt("Enter contact wallet ID: ");
		
		MenuOptionData removeContactOp = new MenuOptionData(ClientActions.REMOVE_CONTACT.ordinal(), "Remove contact", null);
		removeContactOp.addPrompt("Enter contact name: ");

		MenuOptionData listContactsOp = new MenuOptionData(ClientActions.LIST_CONTACTS.ordinal(), "List contacts", null);
		
		MenuOptionData viewWalletOp = new MenuOptionData(ClientActions.VIEW_WALLET_INFO.ordinal(), "View Wallet Info", null);
		
		MenuOptionData viewTransactionsOp = new MenuOptionData(ClientActions.VIEW_TRANSACTIONS.ordinal(), "View Transactions", null);
		
		MenuOptionData logoutOp = new MenuOptionData(ClientActions.LOGOUT.ordinal(), "Logout", null);
		
		MenuData submenu = new MenuData();
		submenu.addOption(addContactOp);
		submenu.addOption(removeContactOp);
		submenu.addOption(listContactsOp);
		submenu.addOption(viewWalletOp);
		submenu.addOption(viewTransactionsOp);
		submenu.addOption(logoutOp);
		
		clientOp.setSubmenu(submenu);
		clientMenu = new MenuData();
		clientMenu.addOption(clientOp);
	}
	
	public Wallet getWallet() {
		return wallet;
	}

	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}
	
	private void login() {
		PSMessage walletMsg = new PSMessage(PSMessage.MessageType.WALLET, this.wallet);
		
		try {
			this.messageIO.writeMessage(this.socket.getOutputStream(), walletMsg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void logout() {
		try {
			this.socket.close();
			this.storeWallet();
			this.storeContacts();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private void sendConfirmationResponse(ConfirmationChoice choice, TransactionData transaction) {
		ConfirmationResponseData data = new ConfirmationResponseData(choice, transaction);
		PSMessage responseMsg = new PSMessage(PSMessage.MessageType.CONFIRMATION_RESPONSE, data);
		
		try {
			this.messageIO.writeMessage(this.socket.getOutputStream(), responseMsg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void displayMenu(MenuData menu) {
		for (MenuOptionData opt : menu.getOptions()) {
			System.out.println("[" + opt.getMenuId() + "]: " + opt.getDisplay());
		}
	}
	
	/**
	 * 
	 * @param menu
	 * @param bin BufferReader
	 * @return The choice entered by the user as an integer. A negative choice indicates the user wants to disconnect
	 * @throws IOException
	 */
	private int readMenuChoice(MenuData menu, BufferedReader bin) throws IOException {
		String line;
		
		line = bin.readLine();

		int choice = Integer.parseInt(line);
		
		boolean choiceValid = false;
		
		if (choice == 0) {
			choiceValid = true;
		} else {
			for (MenuOptionData opt : menu.getOptions()) {
				if (opt.getMenuId() == choice) {
					choiceValid = true;
					break;
				}
			}
		}
		
		while (!choiceValid) {
			System.out.println(choice + " is not an option. Try again.");
			
			line = bin.readLine();
			choice = Integer.parseInt(line);
			
			if (choice == 0) {
				return choice;
			}
			
			choiceValid = false;
			
			if (choice < 0) {
				choiceValid = true;
			} else {
				for (MenuOptionData opt : menu.getOptions()) {
					if (opt.getMenuId() == choice) {
						choiceValid = true;
						break;
					}
				}
			}
		}
		
		return choice;
	}
	
	private void processMenu(MenuData menu, Integer choice, BufferedReader bin) throws IOException {
		
		final int finalChoice;
		if (choice == null) { 
			finalChoice = this.readMenuChoice(menu, bin);
		}
		else {
			finalChoice = choice;
		}
		
		String line;
		
		MenuOptionData option = menu.getOptions()
				.stream()
				.filter(op -> op.getMenuId() == finalChoice)
				.findFirst()
				.orElseGet(() -> {
					System.out.println(new Gson().toJson(menu));
					return null;
				});
		if (option.getSubmenu() == null) {
			for (int i = 0; i < option.getPrompts().size(); i++) {
				System.out.println(option.getPrompts().get(i));
				line = bin.readLine();
				String response = option.getResponse();
				response = response.replace("<" + i + ">", line);
				option.setResponse(response);
			}
			
			//Don't include size, added automatically in writeUnformatted()
			this.messageIO.writeUnformatted(this.socket.getOutputStream(), option.getResponse().getBytes());
		} else {
			this.displayMenu(option.getSubmenu());
			this.processMenu(option.getSubmenu(), null, bin);
		}
	
	}
	
	private void processClientMenu(int choice, BufferedReader bin) throws IOException {
		//get the client menu option
		MenuOptionData option = this.clientMenu.getOptions()
				.stream()
				.filter(op -> op.getMenuId() == choice)
				.findFirst()
				.get();
		MenuData submenu = option.getSubmenu();
		
		this.displayMenu(submenu);
		
		//get the chosen client option
		final int nextChoice = this.readMenuChoice(submenu, bin);
		MenuOptionData nextOption = submenu.getOptions()
				.stream()
				.filter(op -> op.getMenuId() == nextChoice)
				.findFirst()
				.get();
		
		String name = "";
		
		if (nextOption.getSubmenu() == null) {
			//Client menu should never have a submenu
			ClientActions action = ClientActions.values()[nextChoice];
			switch (action) {
			case ADD_CONTACT:
				//get the name and wallet id of the contact
				System.out.println(nextOption.getPrompts().get(0));
				name = bin.readLine();
				System.out.println(nextOption.getPrompts().get(1));
				String intStr = bin.readLine();
				
				//add the contact to the list and store them to the contacts file
				Long id = new Long(intStr);
				this.contacts.put(name, id);
				this.storeContacts();
				System.out.println();
				break;
			case REMOVE_CONTACT:
				//get name of contact to remove 
				System.out.println(nextOption.getPrompts().get(0));
				name = bin.readLine();
				
				if (this.contacts.containsKey(name)) {
					this.contacts.remove(name);
					this.storeContacts();
				}
				System.out.println();
				break;
			case LIST_CONTACTS:
				this.contacts.forEach((cName, cId) -> {
					System.out.println(cName + " with wallet id of " + cId);
				});		
				System.out.println();
				break;
			case VIEW_WALLET_INFO:
				System.out.println("Wallet ID: " + this.wallet.getID());
				System.out.println("Wallet Balance: $" + String.format("%.2f", this.wallet.getBalance()/100.0));
				System.out.println();
				break;
			case VIEW_TRANSACTIONS:
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
				//Sort the transactions by date and print them
				this.wallet.getTransactions()
					.entrySet()
					.stream()
					.sorted((first, second) -> first.getValue().getCreatedAt().compareTo(second.getValue().getCreatedAt()))
					.forEach((entry) -> {
						Transaction trans = entry.getValue();
						String dateStr = formatter.format(LocalDateTime.ofInstant(trans.getCreatedAt(), ZoneId.systemDefault()));
						String from = "" + trans.getSourceWalletId();
						
						//set the from string to contact name if found or "you" if applicable
						if (trans.getSourceWalletId() == this.wallet.getID()) {
							from = "you";
						} else if (this.contacts.containsValue(trans.getSourceWalletId())) {
							Set<Entry<String, Long>> contactEntries = this.contacts.entrySet();
							for (Entry<String, Long> contact : contactEntries) {
								if (contact.getValue() == trans.getSourceWalletId()) {
									from = contact.getKey();
									break;
								}
							}
						}
						
						//set the to string to contact name if found or "you" if applicable
						String to = "" + trans.getDestWalletId();
						if (trans.getDestWalletId() == this.wallet.getID()) {
							to = "you";
						} else if (this.contacts.containsValue(trans.getDestWalletId())) {
							Set<Entry<String, Long>> contactEntries = this.contacts.entrySet();
							for (Entry<String, Long> contact : contactEntries) {
								if (contact.getValue() == trans.getDestWalletId()) {
									to = contact.getKey();
									break;
								}
							}
						}
						
						//print transaction info
						System.out.print(dateStr + " - " + trans.getType().name() + " transaction of $" 
							+ String.format("%.2f", trans.getAmount()/100.0) + " from " + from + " to " + to);
						
						//Print status
						switch (trans.getStatus()) {
						case CREATED:
							System.out.println( " (Created by " + from + ")");
							break;
						case DEST_CONFIRMED:
							System.out.println( " (Confirmed by " + to + ")");
							break;
						case DEST_DENIED:
							System.out.println( " (Rejected by " + to + ")");
							break;
						case DEST_INIT:
						case DEST_PENDING:
						case SOURCE_CONFIRMED:
							System.out.println( " (Awaiting confirmation from " + to + ")");
							break;
						case SOURCE_DENIED:
							System.out.println( " (Cancelled by " + from + ")");
							break;
						case SOURCE_INIT:
						case SOURCE_PENDING:
							System.out.println(" (Awaiting confirmation from " + from + ")");
							break;
						default:
							break;
						}
					});
				System.out.println();
				break;
			case LOGOUT:
				this.logout();
				break;
			
			}
			
		}
		
	}
	
	public void storeContacts() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String contents = gson.toJson(this.contacts);
		
		Path path = Paths.get("wallet-" + this.wallet.getID() + "-contacts.txt");
		 
		try (BufferedWriter writer = Files.newBufferedWriter(path))
		{
		    writer.write(contents);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readContacts() {
		Path path = Paths.get("wallet-" + this.wallet.getID() + "-contacts.txt");
		if (Files.exists(path)) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			
			try {
				String content = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
				Type contactsType = new TypeToken<Map<String, Long>>(){}.getType();
				this.contacts = gson.fromJson(content, contactsType);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	public void storeWallet() {
		if (this.wallet.getID() != 0) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String contents = gson.toJson(this.wallet);
			
			Path path = Paths.get("wallet-" + this.wallet.getID() + ".txt");
			 
			//Use try-with-resource to get auto-closeable writer instance
			try (BufferedWriter writer = Files.newBufferedWriter(path))
			{
			    writer.write(contents);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void readWallet() {
		Path path = Paths.get("wallet-" + this.wallet.getID() + ".txt");
		if (this.wallet.getID() != 0 && Files.exists(path)) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			
			try {
				String content = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
				this.wallet = gson.fromJson(content, Wallet.class);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
	
	private void process(PSMessage msg) {
		AbstractData data = msg.getData();
		
		BufferedReader bin = new BufferedReader(new InputStreamReader(System.in));
		String line;
		
		switch (msg.getMessageType()) {
		case MENU:
			
			MenuData menu = (MenuData)data;
			try {
				//Separate client and server menu. Option 0 of main menu is reserved for client 
				this.displayMenu(this.clientMenu);
				this.displayMenu(menu);
				
				//Keep displaying menu if they keep choosing client options
				int choice = this.readMenuChoice(menu, bin);
				while (choice == 0) {
					this.processClientMenu(choice, bin);
					
					//if logged out
					if (this.socket.isClosed()) {
						return;
					}
					
					this.displayMenu(this.clientMenu);
					this.displayMenu(menu);
					
					choice = this.readMenuChoice(menu, bin);
				}

				this.processMenu(menu, choice, bin);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			break;
		case CONFIRMATION_REQUEST:
			ConfirmationRequestData reqData = (ConfirmationRequestData)data;
			ConfirmationChoice[] choices = reqData.getChoices();
			
			System.out.println("Please confirm the " + reqData.getTransaction().getType() + " transaction request.");
			for (int i = 0; i < choices.length; i++) {
				System.out.println("[" + i + "] : " + choices[i]);
			}
			
			try {
				line = bin.readLine();
				int choice = Integer.parseInt(line);
				
				this.sendConfirmationResponse(choices[choice], reqData.getTransaction());
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			break;
		case TRANSACTION_ERROR:
			TransactionErrorData errData = (TransactionErrorData)data;
			System.out.println("ERROR: " + errData.getMessage());
			System.out.println();
			break;
		case TRANSACTION:
			TransactionData transactionData = (TransactionData)data;
			Transaction transaction = new Transaction(
					transactionData.getID(),
					transactionData.getType(),
					transactionData.getAmount(),
					transactionData.getSourceId(),
					transactionData.getDestinationId(),
					transactionData.getStatus(),
					transactionData.getCreatedAt()
			);
			this.wallet.updateTransaction(transaction);
			this.storeWallet();
			break;
		case WALLET:
			WalletData walletData = (WalletData)data;
			
			//if new wallet, set id to the newly generated id
			if (this.wallet.getID() == 0) {
				this.wallet.setID(walletData.getID());
			}
			
			this.wallet.setBalance(walletData.getBalance());
			this.storeWallet();
			
			break;
		default:
			break;
		}
	}
	
	/**
	 * Process all of the messages received. If a menu is received, process it last.
	 * 
	 * @param messages The list of messages received from the server
	 */
	public void process(PSMessageList messages) {
		Optional<PSMessage> menu = Optional.empty(); 
		for (PSMessage msg : messages.getMessages()) {
			if (msg.getMessageType() == PSMessage.MessageType.MENU) {
				menu = Optional.of(msg);
			} else {
				this.process(msg);
			}
		}
		
		//If there was a menu, process it
		menu.ifPresent(menuMsg -> this.process(menuMsg));
	}
	
	
	
	public static void main(String[] args) {
		String hostname = args[0];
		int port = Integer.parseInt(args[1]);		
		int id = 0;
		if (args.length == 3) {
			id = Integer.parseInt(args[2]);
		}
		
		Socket soc;
		
		Gson gson = new GsonBuilder()
				.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
				.registerTypeAdapter(PSMessage.class, new PSMessageAdapter())
				.registerTypeAdapter(PSMessageList.class, new PSMessageListAdapter())
				.create();
		
		PSJsonSerializer ser = new PSJsonSerializer(gson);
		
		PSMessageIO msgIO = new PSMessageIO(ser);
		
				
		try {
			soc = new Socket(hostname, port);
			Client client = new Client(soc, msgIO);
			//initialize wallets with balance of $20.00
			client.getWallet().setBalance(2000);
			client.getWallet().setID(id);
			
			if (id != 0) {
				client.readWallet();
				client.readContacts();
			}
			
			client.login();
			
			PSMessageList messages;
			while (!soc.isClosed()) {
				messages = msgIO.readMessageList(soc.getInputStream());
				
				client.process(messages);
			}
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
