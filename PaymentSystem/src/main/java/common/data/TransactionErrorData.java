package common.data;

public class TransactionErrorData extends AbstractData {
	
	private String message;

	private long transactionId;
	
	public TransactionErrorData() {
		this.message = "There was an error.";
		this.transactionId = 0;
	}
	
	public TransactionErrorData(String message, long transactionId) {
		super();
		this.message = message;
		this.transactionId = transactionId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	
	
}
