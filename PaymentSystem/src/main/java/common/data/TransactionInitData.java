package common.data;

import common.entities.Transaction;

public class TransactionInitData extends AbstractData {
	
	private long sourceId;
	
	private long destinationId;
	
	private Transaction.TransactionType type;
	
	private long amount;
	
	public TransactionInitData() {
		this.sourceId = 0;
		this.destinationId = 0;
		this.type = Transaction.TransactionType.UNKNOWN;
		this.amount = 0;
	}

	public TransactionInitData(long sourceId, long destinationId, Transaction.TransactionType type, long amount) {
		this.sourceId = sourceId;
		this.destinationId = destinationId;
		this.type = type;
		this.amount = amount;
	}

	public long getSourceId() {
		return sourceId;
	}

	public void setSourceId(long sourceId) {
		this.sourceId = sourceId;
	}

	public long getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(long destinationId) {
		this.destinationId = destinationId;
	}

	public Transaction.TransactionType getType() {
		return type;
	}

	public void setType(Transaction.TransactionType type) {
		this.type = type;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}
	
	
	
	
}
