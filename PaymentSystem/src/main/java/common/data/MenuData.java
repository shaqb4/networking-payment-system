package common.data;

import java.util.ArrayList;
import java.util.List;

public class MenuData extends AbstractData{

	private List<MenuOptionData> options;
	
	public MenuData() {
		this.options = new ArrayList<>();
	}
	
	public void addOption(MenuOptionData opt) {
		this.options.add(opt);
	}

	public List<MenuOptionData> getOptions() {
		return options;
	}

	public void setOptions(List<MenuOptionData> options) {
		this.options = options;
	}
}
