package common.data;

import java.time.Instant;

import common.entities.Transaction;
import common.entities.Transaction.TransactionStatus;
import common.entities.Transaction.TransactionType;

public class TransactionData extends AbstractData {

	private long ID;
	
	private Transaction.TransactionType type;
	
	private long amount;
	
	private long sourceId;
	
	private long destinationId;
	
	private Transaction.TransactionStatus status;
	
	private Instant createdAt;
	
	public TransactionData() {
		this.ID = 0;
		this.type = Transaction.TransactionType.UNKNOWN;
		this.amount = 0;
		this.sourceId = 0;
		this.destinationId = 0;
		this.status = Transaction.TransactionStatus.CREATED;
		this.setCreatedAt(Instant.now());
	}
	
	public TransactionData(long Id, TransactionType type, long amount, long sourceId, long destinationId,
			TransactionStatus status, Instant instant) {
		ID = Id;
		this.type = type;
		this.amount = amount;
		this.sourceId = sourceId;
		this.destinationId = destinationId;
		this.status = status;
		this.setCreatedAt(instant);
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public Transaction.TransactionType getType() {
		return type;
	}

	public void setType(Transaction.TransactionType type) {
		this.type = type;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public long getSourceId() {
		return sourceId;
	}

	public void setSourceId(long sourceId) {
		this.sourceId = sourceId;
	}

	public long getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(long destinationId) {
		this.destinationId = destinationId;
	}

	public Transaction.TransactionStatus getStatus() {
		return status;
	}

	public void setStatus(Transaction.TransactionStatus status) {
		this.status = status;
	}

	public Instant getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Instant createdAt) {
		this.createdAt = createdAt;
	}
}
