package common.data;


public class ConfirmationResponseData extends AbstractData {

	private ConfirmationChoice choice;
	
	private TransactionData transaction;
	
	public ConfirmationResponseData() {
		this.choice = ConfirmationChoice.LATER;
		this.transaction = new TransactionData();
	}
	
	public ConfirmationResponseData(ConfirmationChoice choice, TransactionData trans) {
		this.choice = choice;
		this.transaction = trans;
	}

	public ConfirmationChoice getChoice() {
		return choice;
	}

	public void setChoice(ConfirmationChoice choice) {
		this.choice = choice;
	}

	public TransactionData getTransaction() {
		return transaction;
	}

	public void setTransaction(TransactionData transaction) {
		this.transaction = transaction;
	}
}
