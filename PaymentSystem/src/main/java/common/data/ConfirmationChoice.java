package common.data;

public enum ConfirmationChoice {
	YES,
	NO,
	LATER
}
