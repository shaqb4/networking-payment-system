package common.data;

import java.util.ArrayList;
import java.util.List;

import common.message.MenuOptionBuilder;

public class MenuOptionData extends AbstractData {
	private int menuId;
	
	private String display;
	
	private List<String> prompts;
	
	private String response;
	
	private MenuData submenu;
	
	public MenuOptionData(int id, String disp, String response) {
		this.menuId = id;
		this.display = disp;
		this.response = response;
		this.prompts = null;
		this.submenu = null;
	}
	
	public void addPrompt(String prompt) {
		if (this.prompts == null) {
			this.prompts = new ArrayList<>();
		}
		this.prompts.add(prompt);
	}
	
	public void addPrompts(List<String> prompts) {
		if (this.prompts == null) {
			this.prompts = new ArrayList<>();
		}
		if (prompts != null && !prompts.isEmpty()) {
			this.prompts.addAll(prompts);
		}
		
	}

	public MenuData getSubmenu() {
		return submenu;
	}

	public void setSubmenu(MenuData submenu) {
		this.submenu = submenu;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public List<String> getPrompts() {
		return prompts;
	}

	public void setPrompts(List<String> prompts) {
		this.prompts = prompts;
	}
	
	public static MenuOptionBuilder builder() {
		return new MenuOptionBuilder();
	}
	
}
