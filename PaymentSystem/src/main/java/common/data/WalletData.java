package common.data;

public class WalletData extends AbstractData {

	private long ID;
	
	private long balance;
	
	public WalletData() {
		this.ID = 0;
		this.balance = 0;
	}
	
	public WalletData(long Id, long balance) {
		ID = Id;
		this.balance = balance;
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public long getBalance() {
		return balance;
	}

	public void setBalance(long balance) {
		this.balance = balance;
	}
}
