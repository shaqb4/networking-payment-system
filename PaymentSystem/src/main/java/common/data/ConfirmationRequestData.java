package common.data;

public class ConfirmationRequestData extends AbstractData {

	private TransactionData transaction;
	
	private ConfirmationChoice[] choices;
	
	public ConfirmationRequestData() {
		this.transaction = new TransactionData();
		this.choices = ConfirmationChoice.values();
	}
	
	public ConfirmationRequestData(TransactionData transaction, ConfirmationChoice[] choices) {
		this.transaction = transaction;
		this.choices = choices;
	}

	public TransactionData getTransaction() {
		return transaction;
	}

	public void setTransaction(TransactionData transaction) {
		this.transaction = transaction;
	}

	public ConfirmationChoice[] getChoices() {
		return choices;
	}

	public void setChoices(ConfirmationChoice[] choices) {
		this.choices = choices;
	}
}
