package common.entities;

import common.data.AbstractData;

public interface Sendable {
	 public AbstractData getData();
}
