package common.entities;

import java.util.Hashtable;
import java.util.Map;

import common.data.WalletData;

public class Wallet implements Sendable {
	
	protected long ID;
	
	protected long balance;
	
	protected Map<Long, Transaction> transactions;
	
	public Wallet() {
		this.ID = 0;
		this.balance = 0;
		this.transactions = new Hashtable<>();
	}
	
	public Wallet(long id, long bal, Map<Long, Transaction> transactions) {
		this.ID = id;
		this.balance = bal;
		this.transactions = transactions;
	}

	@Override
	public WalletData getData() {
		return new WalletData(this.ID, this.balance);
	}

	public long getID() {
		return ID;
	}

	public void setID(long id) {
		ID = id;
	}

	public long getBalance() {
		return balance;
	}

	public void setBalance(long balance) {
		this.balance = balance;
	}

	public Map<Long, Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(Map<Long, Transaction> transactions) {
		this.transactions = transactions;
	}
	
	public void updateTransaction(Transaction transaction) {
		this.transactions.put(transaction.getID(), transaction);
	}
}
