package common.entities;

import java.time.Instant;

import common.data.TransactionData;

public class Transaction implements Sendable {
	
	public static enum TransactionType { BILL, PAYMENT, UNKNOWN };
	
	public static enum TransactionStatus { 
		SOURCE_INIT, SOURCE_DENIED, SOURCE_PENDING, SOURCE_CONFIRMED, DEST_INIT, DEST_DENIED, DEST_CONFIRMED, DEST_PENDING, CREATED
	};
	
	protected long ID;
	
	protected TransactionType type;
	
	protected long amount;
	
	protected long sourceWalletId;
	
	protected long destWalletId;
	
	protected TransactionStatus status;
	
	protected Instant createdAt;
	
	public Transaction() {
		this.ID = 0;
		this.type = TransactionType.UNKNOWN;
		this.amount = 0;
		this.sourceWalletId = 0;
		this.destWalletId = 0;
		this.status = TransactionStatus.CREATED;
		this.createdAt = Instant.now();
	}
	
	public Transaction(long id, TransactionType type, long amount, long sourceId, long destId, TransactionStatus stat, Instant instant) {
		this.ID = id;
		this.type = type;
		this.amount = amount;
		this.sourceWalletId = sourceId;
		this.destWalletId = destId;
		this.status = stat;
		this.createdAt = instant;
	}

	@Override
	public TransactionData getData() {
		return new TransactionData(this.ID, this.type, this.amount, this.sourceWalletId, this.destWalletId, this.status, this.createdAt);
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public long getSourceWalletId() {
		return sourceWalletId;
	}

	public void setSourceWalletId(long sourceWalletId) {
		this.sourceWalletId = sourceWalletId;
	}

	public long getDestWalletId() {
		return destWalletId;
	}

	public void setDestWalletId(long destWalletId) {
		this.destWalletId = destWalletId;
	}

	public TransactionStatus getStatus() {
		return status;
	}

	public void setStatus(TransactionStatus status) {
		this.status = status;
	}

	public Instant getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Instant createdAt) {
		this.createdAt = createdAt;
	}
	

}
