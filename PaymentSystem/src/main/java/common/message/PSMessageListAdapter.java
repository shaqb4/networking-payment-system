package common.message;

import java.lang.reflect.Type;
import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import common.data.AbstractData;
import common.data.ConfirmationRequestData;
import common.data.ConfirmationResponseData;
import common.data.MenuData;
import common.data.TransactionData;
import common.data.TransactionErrorData;
import common.data.TransactionInitData;
import common.data.WalletData;

/**
 * 
 * Class used to tell Gson how to serialize a PSMessageList.
 * This does use java specific reflection, but the same functionality should be achievable with non-Java libraries
 * with a little more work. 
 *
 */
public class PSMessageListAdapter implements JsonDeserializer<PSMessageList>, JsonSerializer<PSMessageList> {

	@Override
	public JsonElement serialize(PSMessageList src, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject obj = new JsonObject();
		JsonArray msgElements = context.serialize(src.getMessages()).getAsJsonArray();
		Iterator<PSMessage> it = src.getMessages().iterator();
		int i = 0;
		while (it.hasNext()) {			
			PSMessage msg = it.next();
			
			obj.add(msg.getMessageType().toString(), msgElements.get(i).getAsJsonObject().get(msg.getMessageType().toString()));
			i++;
		}
		return obj;
	}

	@Override
	public PSMessageList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		PSMessageList container = new PSMessageList();
		
		for (String msgType : json.getAsJsonObject().keySet()) {
			PSMessage.MessageType type = PSMessage.MessageType.valueOf(msgType.toUpperCase());
			Type dataType = null;
			switch (type) {
			case TRANSACTION_INIT:
				dataType = TransactionInitData.class;
				break;
			case CONFIRMATION_REQUEST:
				dataType = ConfirmationRequestData.class;
				break;
			case CONFIRMATION_RESPONSE:
				dataType = ConfirmationResponseData.class;
				break;
			case TRANSACTION_ERROR:
				dataType = TransactionErrorData.class;
				break;
			case TRANSACTION:
				dataType = TransactionData.class;
				break;
			case WALLET:
				dataType = WalletData.class;
				break;
			case MENU:
				dataType = MenuData.class;
				break;
			}
			
			container.addMessage(new PSMessage(type, (AbstractData)context.deserialize(json.getAsJsonObject().get(msgType), dataType)));
		}
		
		
		return container;
	}

}
