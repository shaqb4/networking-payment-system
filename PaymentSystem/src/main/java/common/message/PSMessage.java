package common.message;

import common.data.AbstractData;
import common.entities.Sendable;

/**
 * PSMessage - Payment System Message
 * 
 * @author Shahked
 *
 */
public class PSMessage extends AbstractMessage {
	public static enum MessageType { 
		TRANSACTION_INIT,
		TRANSACTION_ERROR,
		CONFIRMATION_REQUEST,
		CONFIRMATION_RESPONSE,
		TRANSACTION,
		WALLET,
		MENU;
		
		@Override public String toString() {
			return name().toLowerCase();
		}
	}
	
	private AbstractData data;
	
	private PSMessage.MessageType messageType;
	
	public PSMessage() {
		this.messageType = null;
		this.data = null;
	}

	public PSMessage(PSMessage.MessageType msgType) {
		this.messageType = msgType;
		this.data = null;
	}
	
	public PSMessage(PSMessage.MessageType msgType, AbstractData data) {
		this.messageType = msgType;
		this.data = data;
	}
	
	public PSMessage(PSMessage.MessageType msgType, Sendable entity) {
		this.messageType = msgType;
		this.data = entity.getData();
	}
	
	public MessageType getMessageType() {
		return this.messageType;
	}
	
	public void setData(AbstractData data) {
		this.data = data;
	}
	
	public void setData(Sendable entity) {
		this.data = entity.getData();
	}
	
	public AbstractData getData() {
		return this.data;
	}
}
