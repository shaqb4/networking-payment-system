package common.message;

public interface PSMessageSerializer extends MessageSerializer {
	
	@Override
	public PSMessage deserialize(String json);
	
	public PSMessageList deserializeList(String json);
}
