package common.message;

import java.lang.reflect.Type;
import java.util.Iterator;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import common.data.AbstractData;
import common.data.ConfirmationRequestData;
import common.data.ConfirmationResponseData;
import common.data.MenuData;
import common.data.TransactionData;
import common.data.TransactionErrorData;
import common.data.TransactionInitData;
import common.data.WalletData;

/**
 * 
 * Class used to tell Gson how to serialize a PSMessage
 * This does use java specific reflection, but the same functionality should be achievable with non-Java libraries
 * with a little more work.
 */
public class PSMessageAdapter implements JsonDeserializer<PSMessage>, JsonSerializer<PSMessage> {


	@Override	
	public JsonElement serialize(PSMessage src, Type typeOfSrc, JsonSerializationContext context) {
	    //return new JsonPrimitive(src.toString());
		  JsonObject obj = new JsonObject();
		  obj.add(src.getMessageType().toString(), context.serialize(src.getData()));
		  return obj;
	  }

	@Override
	public PSMessage deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		// TODO Auto-generated method stub
		Iterator<String> it = json.getAsJsonObject().keySet().iterator();
		PSMessage.MessageType type = PSMessage.MessageType.valueOf(it.next().toUpperCase());
		
		Type dataType = null;
		switch (type) {
		case TRANSACTION_INIT:
			dataType = TransactionInitData.class;
			break;
		case CONFIRMATION_REQUEST:
			dataType = ConfirmationRequestData.class;
			break;
		case CONFIRMATION_RESPONSE:
			dataType = ConfirmationResponseData.class;
			break;
		case TRANSACTION_ERROR:
			dataType = TransactionErrorData.class;
			break;
		case TRANSACTION:
			dataType = TransactionData.class;
			break;
		case WALLET:
			dataType = WalletData.class;
			break;
		case MENU:
			dataType = MenuData.class;
			break;
		}
		
		return new PSMessage(type, (AbstractData)context.deserialize(json.getAsJsonObject().get(type.toString()), dataType));
	}

}
