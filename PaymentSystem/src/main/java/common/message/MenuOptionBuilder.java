package common.message;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonObject;

import common.data.MenuData;
import common.data.MenuOptionData;

public class MenuOptionBuilder {
	private Integer menuId;
	
	private String display;
	
	private List<String> prompts;
	
	private List<Boolean> isNum;
	
	private String response;
	
	private JsonObject responseJson;
	
	private MenuData submenu;
	
	public MenuOptionBuilder() {
		this.menuId = null;
		this.prompts = null;
		this.response = null;
		this.submenu = null;
		this.responseJson = null;
	}
	
	public MenuOptionBuilder withId(int id) {
		this.menuId = id;
		return this;
	}
	
	public MenuOptionBuilder withDisplay(String display) {
		this.display = display;
		return this;
	}
	
	private void addPrompt(String prompt, String field, boolean isNumber) {
		if (this.prompts == null) {
			this.prompts = new ArrayList<>();
			this.isNum = new ArrayList<>();
		}
		String[] fieldPath = field.split("\\.");
		JsonObject el = this.responseJson;
		for (int i = 0; i < fieldPath.length - 1; i++) {
			el = el.getAsJsonObject(fieldPath[i]);
		}
		el.addProperty(fieldPath[fieldPath.length - 1], "<"+this.prompts.size()+">");
		this.prompts.add(prompt);
		this.isNum.add(isNumber);
	}
	
	public MenuOptionBuilder withPrompt(String prompt, String field) {
		this.addPrompt(prompt, field, false);
		
		return this;
	}
	
	public MenuOptionBuilder withNumberPrompt(String prompt, String field) {
		this.addPrompt(prompt, field, true);
		
		return this;
	}
	
	public MenuOptionBuilder withResponse(JsonObject respJson) {
		this.responseJson = respJson;
		
		return this;
	}
	
	public MenuOptionBuilder withSubmenu(MenuData menu) {
		this.submenu = menu;
		
		return this;
	}
	
	public MenuOptionData build() {
		if (this.responseJson != null && this.prompts != null) {
			this.response = this.responseJson.toString();
			for (int i = 0; i < this.prompts.size(); i++) {
				if (this.isNum.get(i) == true) {
					this.response = this.response.replace("\"<"+i+">\"", "<"+i+">");
				}
			}
			
		}
		
		MenuOptionData op = new MenuOptionData(this.menuId, this.display, this.response);
		op.addPrompts(this.prompts);
		
		op.setSubmenu(this.submenu);
		
		return op;
	}
}
