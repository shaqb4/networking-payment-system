package common.message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class PSMessageIO implements PSMessageReader, MessageWriter {

	private PSMessageSerializer serializer;
	
	public PSMessageIO(PSMessageSerializer serializer) {
		this.serializer = serializer;
	}

	@Override
	public void write(OutputStream out, byte[] data) throws IOException {
		out.write(data);
		out.flush();
	}
	
	@Override
	public byte[] read(InputStream in, int size) throws IOException {
		byte ret[] = new byte[size];
		
		int read = 0;
		int nleft = size - read;
		while(nleft>0) {
			int r = in.read(ret,read,nleft);
			read += r;
			nleft = size - read;
		}
		
		return ret;
	}
	
	/**
	 * Send raw message data without the size included. writeUnformatted() formats it correctly automatically
	 * 
	 * @param out OutputStream
	 * @param data byte[] to send
	 * @throws IOException
	 */
	public void writeUnformatted(OutputStream out, byte[] data) throws IOException {
		int size = data.length;
		if (size > 9999) {
			throw new IOException("Could not send message:" + size + " bytes is too large. Four digits max");
		}
		String sizeStr = String.format("%04d", size);
		
		String msg = (sizeStr + ":" + new String(data)); 
				
		this.write(out, msg.getBytes());
	}

	@Override
	public void writeMessage(OutputStream out, AbstractMessage msg) throws IOException {
		String serialized = serializer.serialize(msg);
		int size = serialized.getBytes().length;
		if (size > 9999) {
			throw new IOException("Could not send message:" + size + " bytes is too large. Four digits max");
		}
		String sizeStr = String.format("%04d", size);
		byte[] data = (sizeStr + ":" + serialized).getBytes();
		
		this.write(out, data);
	}
	
	private String readMessageData(InputStream in) throws IOException {
		// read size
		byte s[] = new byte[5];
		s = this.read(in,5);
		int size = Integer.parseInt(new String(s,0,4));
		
		// read the cmd:data
		byte d[] = new byte[size];
		d = this.read(in,size);
		
		return new String(d);
	}

	@Override
	public PSMessage readMessage(InputStream in) throws IOException {
		String data = this.readMessageData(in);
		
		PSMessage msg = this.serializer.deserialize(data);
		
		return msg;
	}

	@Override
	public PSMessageList readMessageList(InputStream in) throws IOException {
		String data = this.readMessageData(in);
		
		PSMessageList msg = this.serializer.deserializeList(data);
		
		return msg;
	}
}
