package common.message;

import java.io.IOException;
import java.io.InputStream;

public interface MessageReader {
	public AbstractMessage readMessage(InputStream in) throws IOException;
	
	public byte[] read(InputStream in, int size) throws IOException;
}
