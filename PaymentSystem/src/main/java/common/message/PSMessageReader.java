package common.message;

import java.io.IOException;
import java.io.InputStream;

public interface PSMessageReader extends MessageReader {
	public PSMessageList readMessageList(InputStream in) throws IOException;
}
