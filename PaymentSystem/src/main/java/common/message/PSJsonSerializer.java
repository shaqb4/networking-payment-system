package common.message;

import com.google.gson.Gson;

public class PSJsonSerializer implements PSMessageSerializer {
	private Gson gson;
	
	public PSJsonSerializer(Gson gson) {
		this.gson = gson;
	}
	
	@Override
	public String serialize(AbstractMessage msg) {
		return gson.toJson(msg);
	}

	@Override
	public PSMessage deserialize(String json) {
		return gson.fromJson(json, PSMessage.class);
	}
	
	@Override
	public PSMessageList deserializeList(String json) {
		return gson.fromJson(json, PSMessageList.class);
	}
}
