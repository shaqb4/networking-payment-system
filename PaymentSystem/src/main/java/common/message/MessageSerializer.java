package common.message;

public interface MessageSerializer {
	public String serialize(AbstractMessage msg);
	
	public AbstractMessage deserialize(String json);
}
