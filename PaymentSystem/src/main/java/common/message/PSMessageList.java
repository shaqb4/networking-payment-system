package common.message;

import java.util.LinkedHashSet;
import java.util.Set;

public class PSMessageList extends AbstractMessage {

	private Set<PSMessage> messages;
	
	public PSMessageList() {
		this.messages = new LinkedHashSet<>();
	}
	
	public void addMessage(PSMessage msg) {
		this.messages.add(msg);
	}
	
	public Set<PSMessage> getMessages() {
		return this.messages;
	}
}
