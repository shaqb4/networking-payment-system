package common.message;

import java.io.IOException;
import java.io.OutputStream;

public interface MessageWriter {
	public void write(OutputStream out, byte[] data) throws IOException;
	public void writeMessage(OutputStream out, AbstractMessage msg) throws IOException;
}
